/*
 * Copyright (C) 2018 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Gabriel Krisman Bertazi <krisman@collabora.co.uk>
 */

#include <stdlib.h>
#include <errno.h>

#include "../include/casefold.h"

int sg_augment_index(struct sg *sg)
{
	void *dirents;
	int len;

	/* Double the scatter-gather index size */
	len = sg->sg_len << 1;

	dirents = realloc(sg->dirents, len * sizeof(struct page));
	if (!dirents) {
		return -ENOMEM;
	}
	sg->dirents = dirents;
	sg->sg_len = len;
	return 0;
}

struct page *sg_alloc_page(struct sg *sg)
{
	struct page *page;
	if (sg->npage_dirent >= sg->sg_len) {
		if (sg_augment_index(sg))
			return NULL;
	}

	page = &sg->dirents[sg->npage_dirent];
	page->size = 0;
	page->data = malloc(SGE_SIZE);
	if (!page)
		return NULL;

	sg->npage_dirent++;
	return page;
}

/* Allocate a region wide enough to store a record of rec_len bytes */
struct linux_dirent64 *sg_mem_alloc(struct sg *sg, int rec_len)
{
	struct page *page = &sg->dirents[sg->npage_dirent-1];
	int offset;

	if (page->size + rec_len >= SGE_SIZE)
		page = sg_alloc_page(sg);

	offset = page->size;
	page->size += rec_len;
	return (struct linux_dirent64 *) (((char*)page->data) + offset);
}

int sg_init(struct sg *sg)
{
	int init_len = 10;
	sg->dirents = calloc(init_len, sizeof(struct page));
	if (!sg->dirents)
		return -ENOMEM;

	sg->npage_dirent = 0;
	sg->sg_len = init_len;

	return 0;
}

void sg_free(struct sg *sg)
{
	for (int i = 0; i < sg->npage_dirent; i++)
		free(sg->dirents[i].data);
	free(sg->dirents);
}
