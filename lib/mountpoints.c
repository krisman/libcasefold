/*
 * Copyright (C) 2018 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Gabriel Krisman Bertazi <krisman@collabora.co.uk>
 */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include "../include/casefold.h"

#define PATH_MAX 1024

struct mountpoint {
	struct mountpoint *next;
	int len;
	struct dentry *dentry;
	char key[0];
};

struct mountpoint *mountpoints = NULL;

static struct mountpoint *alloc_mountpoint(const char *path)
{
	struct mountpoint *n;
	int len = strlen(path);

	if (path[len-1] != '/')
		len++;

	n = malloc(sizeof(struct mountpoint) + sizeof(char)*(len + 1));
	if (!n)
		return NULL;

	n->len = len - 1;
	memcpy(n->key, path, len);
	n->key[len] = '\0';
	n->key[len-1] = '/';
	n->next = NULL;
	n->dentry = dentry_create(strdup(n->key), NULL);
	n->dentry->fd = ni_open(n->key, O_DIRECTORY|O_RDONLY, 0);

	/* Mountpoints have the same component and ci_name */
	n->dentry->ci_name = n->dentry->component;

	dcache_add(n->dentry);
	return n;
}

static int insert_mountpoint(const char *path)
{

	struct mountpoint *mountpoint;

	mountpoint = alloc_mountpoint(path);
	if (!mountpoint)
		return -ENOMEM;

	mountpoint->next = mountpoints;
	mountpoints = mountpoint;
	return 0;
}

/*  Quickly check whether a given path has a CI component. If true,
 *  return the index of the first CI element on the path string (slash
 *  included).  Otherwise return negative */

int find_absolute_ci_root(const char *path, struct dentry **mnt_dentry)
{
	struct mountpoint *mnt;
	for (mnt = mountpoints; mnt; mnt = mnt->next)
		if(!strncmp(path, mnt->key, mnt->len)) {
			*mnt_dentry = mnt->dentry;
			return mnt->len;
		}
	return -1;
}

static FILE *try_load_file()
{
	FILE *f;
	char local_file[PATH_MAX];
	char *home;

	f = fopen("/etc/cimounts", "r");
	if (f)
		return f;

	home = getenv("HOME");
	if (!home)
		return NULL;
	snprintf(local_file, PATH_MAX, "%s/.cimounts", home);
	f = fopen(local_file, "r");
	if (f)
		return f;

	return NULL;
}

static int validate_mountpoint(const char *path)
{
	if (!strcmp(path, "/"))
		return -1;
	return 0;
}

int load_mount_points()
{
	FILE *f = NULL;
	char line[PATH_MAX], *envvar, *tmp = NULL, *path = NULL;
	ssize_t len;
	int ret = 0;

	envvar = getenv("CI_MOUNTPOINTS");
	if (envvar) {
		strcpy(line, envvar);

	} else {
		f = try_load_file();
		if (!f) {
			fprintf(stderr, "cimounts file not found.\n");
			return -ENOENT;
		}
		path = line;
	}

	for (;;) {
		if (envvar) {
			if (!path)
				path = strtok_r(line, ":", &tmp);
			else
				path = strtok_r(NULL, ":", &tmp);

			if (!path)
				break;
		} else {
			size_t blen = PATH_MAX;
			len = getline(&path, &blen, f);
			path = line;
			/* fix: Ignore empty lines or lines with single
			 * char. */
			if (len <= 1)
				break;
			if (path[len-1] == '\n')
				path[len-1] = '\0';
		}

		if (validate_mountpoint(path) < 0) {
			fprintf(stderr, "Warning: libcasefold: Can't configure "
				"%s as a CI mountpoint. Ignoring.\n", path);
			continue;
		}

		if (insert_mountpoint(path))
			continue;
		ret++;
	}

	if (!envvar)
		fclose(f);

	return ret;
}
