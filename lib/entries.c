/*
 * Copyright (C) 2018 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Gabriel Krisman Bertazi <krisman@collabora.co.uk>
 */

#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <unicase.h>
#include <stdio.h>

#include <linux/limits.h>

#include "../include/casefold.h"

#define likely(x)      __builtin_expect(!!(x), 1)
#define unlikely(x)    __builtin_expect(!!(x), 0)

#ifdef DEBUG
#define print_debug(str) \
	syscall_no_intercept(SYS_write, 1, str, strlen(str));
#else
#define print_debug(str)
#endif

#define AT_SYSCALL 1

static int ci_lookup_fast(const struct dentry *parent, char *component,
			  char **ci_name)
{
	int i;
	struct linux_dirent64 *dentry;

	for (i = 0; i < parent->sg.npage_dirent; i++) {
		dentry = search_block(component, strlen(component),
				      &parent->sg.dirents[i]);
		if (dentry) {
			*ci_name = dentry->d_name;
			return 1;
		}
	}
	return 0;
}

static int dentry_fill_dirents(struct dentry *dentry)
{
	struct page *page;
	int nread;

	for(;;) {
		page = sg_alloc_page(&dentry->sg);
		if (!page)
			break;

		nread = ni_getdents64(dentry->fd, (char*) page->data,
				      SGE_SIZE);
		if (nread <= 0)
			break;

		page->size = nread;
	}
	return 0;
}

static inline int ci_lookup(struct dentry *parent, char *component,
			    char **ci_name)
{
	if (parent->sg.npage_dirent == 0)
		dentry_fill_dirents(parent);

	return ci_lookup_fast(parent, component, ci_name);
}

static int concat_component_to_path(char *rpath, int offset,
				    const char *component)
{
	int size = strlen(component);

	if ((offset + 1 + size) >= PATH_MAX)
		return -ENAMETOOLONG;

	rpath[offset++] = '/';
	memcpy(rpath+offset, component, size);
	offset += size;
	rpath[offset] = '\0';

	return offset;
}

static int search_ci_path(struct dentry *parent, char *path, char *rpath)
{
	char *saveptr, *component;
	struct dentry *dentry;
	char *ci_name;
	int rpath_off = 0;
	char normalized[PATH_MAX];
	size_t norm_len = PATH_MAX;

	if (unlikely(parent->fd < 0))
		return -ENOENT;

	rpath_off = strlen(parent->component) - 1;
	memcpy(rpath, parent->component, rpath_off);

	component = strtok_r(path, "/", &saveptr);
	while (component) {
		if (component[0] == '.') {
			if (component[1] == '.') {
				/* DOTDOT */

				/* Don't let the user escape the CI
				   mountpoint once he has entered it. */
				if (!parent->parent)
					return -EFAULT;

				dentry = parent->parent;
				ci_name = component;
				goto write_and_fetch_next;
			} else if (component [1] == '\0') {
				dentry = parent;
				ci_name = component;
				goto write_and_fetch_next;
			}
		}
		norm_len = PATH_MAX;
		u8_casexfrm((uint8_t *) component, strlen(component),
			    NULL, UNINORM_NFKD, normalized, &norm_len);

		dentry = dcache_lookup(parent, normalized);
		if (unlikely(!dentry)) {
			dentry = dentry_create(normalized, parent);

			if (unlikely(!ci_lookup(parent, normalized,
						&ci_name))) {
				dentry_destroy(dentry);
				return -ENOENT;
			}

			/* We need te dentry to have the
			 * Case-exact name */
			dentry->ci_name = ci_name;
			dcache_add(dentry);
		} else
			ci_name = dentry->ci_name;

write_and_fetch_next:
		rpath_off = concat_component_to_path(rpath, rpath_off,
						     ci_name);
		if (unlikely(rpath_off < 0))
			return -ENAMETOOLONG;

		/* Fetch next component while eating out empty components
		   like /home//user) */
		do {
			component = strtok_r(NULL, "/", &saveptr);
		} while(component && *component == '\0');

		if (!component)
			break;

		/* parent_fd is not the last directory in the path. we
		 * need to open the next component as a directory and
		 * repeat.  */
		if (dentry->fd <= 0) {
			dentry->fd = openat_directory(parent->fd,
						      dentry->ci_name);
			if (unlikely(dentry->fd < 0))
				return -ENOENT;
		}
		parent = dentry;
	}
	/* there is a CI component with exact path in r. Success*/
	return 0;
}

static int get_dfd_path(int dfd, char *cpath, size_t size)
{
	pid_t pid = getpid();
	char proc_fd[20];

	/* The largest proc_fd can get is limited by
	 * len(MAX_PID)+len(MAX_FD), so len 20 is a good compromise */

	snprintf(proc_fd, 20, "/proc/%d/fd/%d", pid, dfd);
	return ni_readlink(proc_fd, cpath, size);
}

enum {
	TRANSLATION_DONE = 0,
	EXEC_ORIGINAL = 1,
};

/* translate_path() cannot operate on 'path' directly for two reasons:
 * (1) It might point to a read-only memory region in the user
 * application or (2) the user might expect it not to be modified by the
 * glibc wrapper.  Since we can't have assurances for any of these
 * cases, and we absolutely need to modify it during strtok_r(), we copy
 * path to the cpath buffer and only operate on the later.
 *
 * We explore the path to cpath copy to calculate the absolute pathname,
 * which won't be used during lookup for performance reasons, but is
 * still necessary for correctly identifying whether the mountpoint
 * is case-insensitive.*/
static int translate_path(const char *path, char *rpath,
			  unsigned int flags, int dfd)
{
	char cpath[PATH_MAX];
	int r, parent_index;
	struct dentry *parent;
	int base_len = 0;

	if (unlikely(!path))
		return EXEC_ORIGINAL;

	/* If exact-match, use that instead */
	if (ni_faccessat(dfd, path, F_OK, 0x0) != -ENOENT)
		return EXEC_ORIGINAL;

	if (path[0] != '/') {
		int relative_len;

		if (!(flags & AT_SYSCALL) || dfd == AT_FDCWD) {
			char *r;
			/* Path relative to CWD */
			r = getcwd(cpath, PATH_MAX);
			if (!r)
				return -errno;
			base_len = strlen(cpath);
		} else {
			/* Path relative to dfd */
			base_len = get_dfd_path(dfd, cpath, PATH_MAX);
			if (base_len <= 0)
				return -errno;
		}

		relative_len = strlen(path);
		if (unlikely(base_len + relative_len + 2 >= PATH_MAX))
			return -ENAMETOOLONG;
		cpath[base_len++] = '/';
	}

	strcpy(cpath + base_len, path);

	parent_index = find_absolute_ci_root(cpath, &parent);
	if (parent_index < 0)
		return EXEC_ORIGINAL;

	r = search_ci_path(parent, &cpath[parent_index+1], rpath);
	if (r < 0)
		return r;

	 print_debug("Original  : ");
	 print_debug(path);
	 print_debug("\nTranslated: ");
	 print_debug(rpath);
	 print_debug("\n");

	return TRANSLATION_DONE;
}

static int syscall_hook(long syscall_number, long arg0, long arg1,
			long arg2, long arg3,long arg4, long arg5,
			long *result)
{
	int r, dfd = AT_FDCWD;
	unsigned int flags = 0;
	char **path, rpath[PATH_MAX];

	switch (syscall_number)
	{
	case SYS_unlink:
	case SYS_open:
	case SYS_creat:
	case SYS_stat:
	case SYS_lstat:
#ifdef __i686__
	case SYS_lstat64:
	case SYS_stat64:
	case SYS_oldstat:
	case SYS_oldlstat:
#endif
	case SYS_chdir:
	case SYS_mknod:
	case SYS_chmod:
	case SYS_chown:
	case SYS_lchown:
#ifdef __i686__
	case SYS_chown32:
	case SYS_lchown32:
#endif
#ifndef __x86_64__
	case SYS_umount:
#endif
	case SYS_umount2:
	case SYS_utime:
	case SYS_utimes:
	case SYS_access:
	case SYS_mkdir:
	case SYS_rmdir:
	case SYS_acct:
	case SYS_chroot:
	case SYS_readlink:
	case SYS_swapon:
	case SYS_swapoff:
	case SYS_truncate:
#ifdef __i686__
	case SYS_truncate64:
#endif
	case SYS_statfs:
	case SYS_setxattr:
	case SYS_lsetxattr:
	case SYS_getxattr:
	case SYS_lgetxattr:
	case SYS_listxattr:
	case SYS_llistxattr:
	case SYS_removexattr:
	case SYS_lremovexattr:
		path = (char**) &arg0;
		break;
	case SYS_unlinkat:
	case SYS_openat:
#ifndef __i686__
	case SYS_newfstatat:
#else
	case SYS_fstatat64:
#endif
	case SYS_faccessat:
	case SYS_mknodat:
	case SYS_fchmodat:
	case SYS_fchownat:
	case SYS_utimensat:
	case SYS_futimesat:
	case SYS_mkdirat:
	case SYS_readlinkat:
		flags |= AT_SYSCALL;
		dfd = arg0;
		path = (char**) &arg1;
		break;
	default:
		goto exec_syscall;
	}

	inotify_fetch_events();

	r = translate_path(*path, rpath, flags, dfd);
	if (r == TRANSLATION_DONE) {
		*path = rpath;
	} else if (r == EXEC_ORIGINAL || r == -ENOENT) {
		goto exec_syscall;
	} else {
		/* Other error conditions (r < 0) */
		*result = r;
		return 0;
	}
	*result = syscall_no_intercept(syscall_number, arg0, arg1, arg2,
				       arg3, arg4, arg5);
	return 0;

exec_syscall:
	return 1;
}

static __attribute__((constructor)) void init(void)
{
	const int NPAGES = 1;

	dcache_alloc(NPAGES);

	/* No point in intercepting if there is no CI mountpoint.  */
	if (load_mount_points() <= 0)
		return;

	/* Activate interception. */
	intercept_hook_point = syscall_hook;
}
