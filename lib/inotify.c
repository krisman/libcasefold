/*
 * Copyright (C) 2018 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Gabriel Krisman Bertazi <krisman@collabora.co.uk>
 */

#include <sys/inotify.h>
#include <stdlib.h>
#include <string.h>
#include <linux/limits.h>
#include <stdio.h>
#include <errno.h>

#include "../include/casefold.h"

#ifdef DEBUG
#define print_debug(str) \
	syscall_no_intercept(SYS_write, 1, str, strlen(str));
#else
#define print_debug(str)
#endif

struct inotify_instance {
	int fd;
	struct inotify_watcher *watcher_list;
};

struct inotify_instance *main_instance;

struct inotify_watcher {
	struct inotify_instance *instance;
	int wd;
	struct dentry *dentry;
	struct inotify_watcher *next;
};

static inline int ni_inotify_init(int flags)
{
	return syscall_no_intercept(SYS_inotify_init1, flags);
}

static struct inotify_instance *inotify_get_instance()
{
	if (!main_instance) {
		main_instance = malloc(sizeof(struct inotify_instance));
		if (!main_instance)
			return NULL;

		main_instance->fd = ni_inotify_init(IN_NONBLOCK);
		main_instance->watcher_list = NULL;
		if (main_instance->fd == -1) {
			free(main_instance);
			return NULL;
		}
	}
	return main_instance;
}

static char *reconstruct_path(struct dentry *dentry, char *buffer, int bufsiz)
{
	int i = bufsiz - 1;

	buffer[i] = '\0';
	while (dentry) {
		int size = strlen(dentry->ci_name);
		i -= size;
		if (i < 0)
			return NULL;

		memcpy(&buffer[i], dentry->ci_name, size);
		dentry = dentry->parent;
	}

	print_debug(&buffer[i]);
	return &buffer[i];
}

struct inotify_watcher *create_watcher(struct dentry *dentry)
{
	struct inotify_watcher *watcher = malloc(sizeof(struct inotify_watcher));
	char buffer[PATH_MAX], *full_path;

	if (!watcher)
		return NULL;

	full_path = reconstruct_path(dentry, buffer, PATH_MAX);
	if (!full_path) {
		free(watcher);
		return NULL;
	}

	watcher->instance = inotify_get_instance();
	watcher->dentry = dentry;
	watcher->wd = inotify_add_watch(watcher->instance->fd, full_path,
					IN_CREATE|IN_DELETE|IN_DELETE_SELF);
	if (watcher->wd < 0) {
		free(watcher);
		return NULL;
	}

	watcher->next = main_instance->watcher_list;
	main_instance->watcher_list = watcher;

	return watcher;
}

static int remove_from_list(struct inotify_instance *ins,
			    struct inotify_watcher *watcher)
{
	struct inotify_watcher **parent = &ins->watcher_list;
	while (*parent && *parent != watcher)
		parent = &((*parent)->next);

	if (!*parent)
		return -ENOENT;

	*parent = (*parent)->next;
	return 0;
}

/* Memory is freed by this function.  Caller should consider the watcher
   pointer as invalid after calling this function */
void inotify_destroy_watcher(struct inotify_watcher *watcher)
{
	remove_from_list(watcher->instance, watcher);
	inotify_rm_watch(watcher->instance->fd, watcher->wd);
	free(watcher);
}

static struct dentry *get_dentry_by_watcher(const struct inotify_instance *ins,
					    int wd)
{
	struct inotify_watcher *w;

	for (w = ins->watcher_list; w; w = w->next)
		if (w->wd == wd)
			return w->dentry;

	return NULL;
}

static int handle_create_event(const struct inotify_event *event)
{
	struct dentry *dentry = get_dentry_by_watcher(main_instance,
						      event->wd);

	dentry_add_file(dentry, event->name, strlen(event->name));
	return 0;
}

static int handle_remove_event(const struct inotify_event *event)
{
	struct dentry *dentry = get_dentry_by_watcher(main_instance,
						      event->wd);

	dentry_remove_file(dentry, event->name);
	return 0;
}

int inotify_fetch_events()
{
	char *ptr, buffer[BUFSIZ];
	int nread;
	const struct inotify_event *event;

	while(1) {
		nread = ni_read(main_instance->fd, buffer, BUFSIZ);
		if (nread <= 0)
			break;

		for (ptr = buffer; ptr < buffer + nread;
		     ptr += sizeof(struct inotify_event) + event->len) {
			event = (const struct inotify_event *) ptr;
			if (event->mask & IN_CREATE)
				handle_create_event(event);
			if (event->mask & IN_DELETE)
				handle_remove_event(event);
		}
	}
	return 0;
}

