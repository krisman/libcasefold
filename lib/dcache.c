/*
 * Copyright (C) 2018 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Gabriel Krisman Bertazi <krisman@collabora.co.uk>
 */

#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unicase.h>
#include <linux/limits.h>

#include "../include/casefold.h"

#ifdef DEBUG
#define print_debug(str) \
	syscall_no_intercept(SYS_write, 1, str, strlen(str));
#else
#define print_debug(str)
#endif

struct blist {
	struct blist *next;
	struct dentry *dentry;
};

static struct blist **dcache_hashtable = NULL;
static int HASHTABLE_MASK;

static unsigned int dcache_calculate_hash(const char *component)
{
	/* djb2 */
        unsigned int hash = 5381;
        int c;

        while(*component) {
		c = *component;
		hash = hash * 33 ^ c;
		component ++;
	}

        return hash;
}

struct dentry *dcache_lookup(const struct dentry *parent,
			     const char *component)
{
	unsigned int hash = dcache_calculate_hash(component) & HASHTABLE_MASK;
	struct blist *b;
	int len = strlen(component);

	for (b = dcache_hashtable[hash]; b ; b = b->next) {
		if (parent != b->dentry->parent)
			continue;

		if (strlen(b->dentry->component) != len)
			continue;

		if (strcmp(b->dentry->component, component))
			continue;
		return b->dentry;
	}
	return NULL;
}

void dcache_add(struct dentry *dentry)
{
	unsigned int hash = dcache_calculate_hash(dentry->component) & HASHTABLE_MASK;
	struct blist *b = malloc (sizeof(struct blist*));

	b->next = dcache_hashtable[hash];
	b->dentry = dentry;
	dcache_hashtable[hash] = b;

	/* Watcher creation needs to be here instead of at the dentry
	   creation time, because we cannot be sure at any time before
	   now that d->component is correct.  It needs to be the exact
	   match, otherwise create_watcher() will fail to install the
	   watcher. */
	dentry->watcher = create_watcher(dentry);
	if (!dentry->watcher) {
		/* FIXME: Need to handle this case. */
		return;
	}
}

static void dcache_remove(struct dentry *dentry)
{
	struct blist **b, *tmp;
	unsigned int hash = (dcache_calculate_hash(dentry->component)
			     & HASHTABLE_MASK);

	for (b = &dcache_hashtable[hash]; *b ; b = &(*b)->next) {
		if ((*b)->dentry != dentry)
			continue;

		tmp = *b;
		(*b) = tmp->next;
		free(tmp);
		break;
	}
}

static void record_normalize(struct linux_dirent64 *record)
{
	size_t len = PATH_MAX;
	char normrec[len];

	u8_casexfrm((uint8_t *)record->d_name,
		    strlen(record->d_name), NULL,
		    UNINORM_NFKD, normrec, &len);

	record->normalized = malloc(sizeof(struct lstring) + len + 1);
	record->normalized->len = len;
	strncpy(record->normalized->s, normrec, len);
	record->d_type |= D_NORMALIZED;
}

struct linux_dirent64 *search_block(const char *ciname, int ciname_len,
				    struct page *block)
{
	int off;
	struct linux_dirent64 *record;

	char *buffer = (char *) block->data;

	for (off = 0; off < block->size; off += record->d_reclen) {
		record = (struct linux_dirent64 *) (buffer + off);

		if (!(record->d_type & D_NORMALIZED))
			record_normalize(record);

		if (record->normalized->len != ciname_len)
			continue;

		if (!memcmp(record->normalized->s, ciname, ciname_len))
			return record;
	}
	return NULL;
}

void dentry_add_file(struct dentry *dentry, const char *component, int len)
{
	struct linux_dirent64 *record;
	char normalized[PATH_MAX];
	size_t norm_length = PATH_MAX;
	int rec_len;

	u8_casexfrm((uint8_t *) component, strlen(component),
		    NULL, UNINORM_NFKD, normalized, &norm_length);

	rec_len = (sizeof(struct linux_dirent64) + len + 1 +
		   sizeof(struct lstring) + norm_length + 1);

	record = sg_mem_alloc(&dentry->sg, rec_len);

	/* Original name */
	record->d_reclen = rec_len;
	strncpy(record->d_name, component, len+1);

	/* normalized name */
	record->normalized = (struct lstring*) (record->d_name+len+1);
	record->normalized->len = norm_length;
	strncpy(record->normalized->s, normalized, norm_length+1);
	record->d_type |= (D_NORMALIZED|D_INLINE);
}

static void dentry_remove_record(struct dentry *parent,
				 const char *component)
{
	struct linux_dirent64 *record;
	int i;

	for (i = 0; i < parent->sg.npage_dirent; i++) {
		record = search_block(component, strlen(component),
				      &parent->sg.dirents[i]);
		if (record) {
			/* Currently, we cannot reuse the memory of a
			 * record, so we must only mark the record as
			 * empty and skip it during searches.
			 *
			 * No component can have a '/' in its d_name in
			 * a POSIX Filesystem.  So we can use that to
			 * make sure the record will never match
			 * anything. */
			record->d_name[0] = '/';
			record->d_type &= ~D_NORMALIZED;
			if (!(record->d_type & D_INLINE))
				free(record->normalized);
			break;
		}
	}
}

void dentry_remove_file(struct dentry *parent, const char *component)
{
	struct dentry *dentry;
	dentry_remove_record(parent, component);

	dentry = dcache_lookup(parent, component);
	if (dentry) {
		dcache_remove(dentry);
		dentry_destroy(dentry);
	}
}

struct dentry *dentry_create(char *component, struct dentry *parent)
{
	struct dentry *d = malloc(sizeof(struct dentry));

	if (!d)
		return NULL;

	d->component = strdup(component);
	d->parent = parent;
	d->watcher = NULL;
	d->fd = 0;

	if (sg_init(&d->sg))
		goto fail_dirents;

	return d;

fail_dirents:
	free(d);
	return NULL;
}

void dentry_destroy(struct dentry *d)
{
	/* d->ci_name name memory is owned by the dentry parent.  We
	 * *cannot* free it here. */
	if (d->watcher)
		inotify_destroy_watcher(d->watcher);

	sg_free(&d->sg);
	free(d->component);

	if (d->fd > 0)
		close(d->fd);
	free(d);
}

int dcache_alloc(int pages)
{
	int nmemb = pages * (4096/sizeof(struct blist *));

	HASHTABLE_MASK = nmemb - 1;

	dcache_hashtable = calloc(nmemb, sizeof(struct blist *));
	if (!dcache_hashtable)
		return -ENOMEM;

	return 0;
}
