#+TITLE: libcasefold - Case-Insensitive pathname dynamic translator.
#+AUTHOR: Gabriel Krisman Bertazi <krisman@collabora.com>
#+OPTIONS: ^:nil

* License

This file is part of libcasefold.
Copyright (C) 2018 Collabora Ltd.
See the file LICENSE.LGPL2.1 for copying conditions.

* Requirements

 - libsyscall_intercept >= 0.1.0: https://github.com/pmem/syscall_intercept
 - GNU libunistring >= 0.9.7: https://www.gnu.org/software/libunistring/
 - Linux kernel >= 2.6.27

* Limitations

Supported only by programs dynamic linked to GNU LibC under GNU/Linux on
x86_64.

Only system calls made through LibC (via specific wrappers or
syscall(2)) will have paths translated.

LD_PRELOAD cannot be used on SUID binaries.  The library will not be
loaded and the program will execute in a Case-sensitive fashion without
warning.  This is a security decision on the LD_PRELOAD behavior, and
there is no workaround for that.  If your program requires SUID, the
only way to use this library is to rebuild your program and dynamic link
to libcasefold at compilation time.

As a design decision, execve() and uselib() syscalls are not supported.
This can change in the future.

* Installation

** From Source

#+BEGIN_SRC sh
$ git clone https://gitlab.collabora.com/krisman/libcasefold.git libcasefold
$ cd libcasefold/
$ mkdir install
$ ./bootstrap.sh
$ ./configure --prefix=$PWD/install
$ make
$ make install
#+END_SRC

* Defining mountpoints

There are two ways to define case-insensitive "mountpoints".  You can
either set the environment variable CI_MOUNTPOINTS, like this:

#+BEGIN_SRC sh
$ export CI_MOUNTPOINTS=/tmp/ci:/home/user/ci2
#+END_SRC

Multiple mountpoints can be defined by using the ':' character as
separator.

The second alternative is creating a configuration file, where each line
is a mountpoint. The configuration file is searched on the following paths:

	- /etc/cimounts
	- ${HOME}/.cimounts

If the /etc/cimounts file exists, it will take precedence over the user
one.

If the CI_MOUNTPOINTS environment variable is defined, the cimounts file
is ignored.

* Usage

To make use of the library without recompiling the target program, the
library needs to be pre-loaded into the target program with LD_PRELOAD.

#+BEGIN_SRC sh
$ LD_PRELOAD=libcasefold.so /bin/cat HELLO_WORLD.txt
#+END_SRC

* Running and Adding new tests

libcasefold uses DejaGNU for automated testing.  To run the test suite, do:

#+BEGIN_SRC sh
make check
#+END_SRC

New tests should be added to the appropriate sub-directory inside
testsuite/.

* Missing features before 1.1 release

** TODO Multi-thread support.
** TODO Handle syscalls with two paths.
  - link
  - linkat
  - mount
  - rename*
  - symlink
  - symlinkat
  - pivot_root
