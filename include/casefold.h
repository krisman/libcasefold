/*
 * Copyright (C) 2018 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Gabriel Krisman Bertazi <krisman@collabora.co.uk>
 */

#ifndef CASEFOLD_H
#define CASEFOLD_H

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <libsyscall_intercept_hook_point.h>
#include <syscall.h>
#include <stdint.h>
#include <fcntl.h>

struct dentry;
struct inotify_watcher;

struct lstring {
	int len;
	char s[];
};

struct linux_dirent64 {
	uint64_t     d_ino;    /* 64-bit inode number */
	union {
		uint64_t d_off;    /* 64-bit offset to next structure */
		struct lstring *normalized;
	};
	unsigned short d_reclen; /* Size of this dirent */
#define D_NORMALIZED	0x80
#define D_INLINE	0x40
	unsigned char  d_type;   /* File type */
	char           d_name[]; /* Filename (null-terminated) */
};

struct dentry {
	char *component;
	char *ci_name;
	unsigned int hash;
	struct dentry *parent;
	int fd;
	struct inotify_watcher *watcher;

	struct sg {
		struct page {
			size_t size;
			struct linux_dirent64 *data;
		} *dirents;

		int npage_dirent;
		int sg_len;
	} sg;
};

/* unintercepted syscall hooks */

static inline int ni_getdents64(unsigned int fd, char *buffer,
				unsigned int size)
{
	return syscall_no_intercept(SYS_getdents64, fd, buffer, size);
}

static inline int ni_faccessat(int dfd, const char *pathname,
			       int mode, int flags)
{
	return syscall_no_intercept(SYS_faccessat, dfd, pathname,
				    mode, flags);
}

static inline ssize_t ni_read(int fd, void *buf, size_t count)
{
	return syscall_no_intercept(SYS_read, fd, buf, count);
}

static inline int ni_readlink(const char *pathname, char* buf, size_t bufsiz)
{
	return syscall_no_intercept(SYS_readlink, pathname, buf, bufsiz);
}

static inline int ni_open(const char *pathname, int flags, int mode)
{
	return syscall_no_intercept(SYS_open, pathname, flags, mode);
}

static inline int ni_openat(int dfd, const char *pathname,
			    int flags, int mode)
{
	return syscall_no_intercept(SYS_openat, dfd, pathname,
				    flags, mode);
}

static inline int open_directory(char *path)
{
	return ni_open(path, O_DIRECTORY|O_RDONLY, 0);
}

static inline int openat_directory(int dfd, char *path)
{
	return ni_openat(dfd, path, O_DIRECTORY|O_RDONLY, 0);
}

/* dcache.h */

int dcache_alloc(int nmemb);
struct dentry *dcache_lookup(const struct dentry *parent,
			     const char *component);
struct dentry *dentry_create(char *component, struct dentry *parent);
void dentry_destroy(struct dentry *dentry);
void dcache_add(struct dentry *dentry);

struct linux_dirent64 *search_block(const char *ciname, int ciname_len,
				    struct page *block);

void dentry_add_file(struct dentry *dentry, const char *component, int len);
void dentry_remove_file(struct dentry *parent, const char *component);

/* mountpoints.h */

int load_mount_points();
int find_absolute_ci_root(const char *path, struct dentry **mnt_dentry);

/* inotify.h */
struct inotify_watcher *create_watcher(struct dentry *dentry);
void inotify_destroy_watcher(struct inotify_watcher  *watcher);
int inotify_fetch_events();

/* sg.h */

#define SGE_SIZE 4096 * 4

int sg_init(struct sg *sg);
void sg_free(struct sg *sg);

struct page *sg_alloc_page(struct sg *sg);
struct linux_dirent64 *sg_mem_alloc(struct sg *sg, int rec_len);
int sg_augment_index(struct sg *sg);

#endif /* CASEFOLD_H */
