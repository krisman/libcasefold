#
# Copyright (C) 2018 Collabora Ltd.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 2.1 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
# Author: Gabriel Krisman Bertazi <krisman@collabora.co.uk>

# Timeout reduced to 3 seconds
set timeout 3

# The name of this test and the command we will run
set test_file "stress.exp"

set basedir "$objdir/libcasefold.stress"

proc pathwalkci {} {
    global basedir test_mnt
    set pathwalkci "$basedir/pathwalkci"
    set pathwalkci_dir "$test_mnt/pathwalkci"

    prepare_test "mkdir" "$pathwalkci_dir"

    set n 1000
    run_ci "pathwalkci CI lookup of 1000 on empty dir" \
	"$pathwalkci" "$pathwalkci_dir $n ci" "*found $n files*"

    # Do pathwalkci lookups from 5k to 50k files in jumps of 5k
    for {set n 5000} {$n <= 50000} {incr n 5000} {
	run_ci "pathwalkci CI lookup of $n files on mixed dir" \
	    "$pathwalkci" "$pathwalkci_dir $n ci" "*found $n files*"
    }

    prepare_test "rm" "$pathwalkci_dir/*"
    prepare_test "rmdir" "$pathwalkci_dir"
}

pathwalkci
