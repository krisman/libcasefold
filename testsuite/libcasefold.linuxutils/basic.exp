#
# Copyright (C) 2018 Collabora Ltd.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 2.1 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
# Author: Gabriel Krisman Bertazi <krisman@collabora.co.uk>
#
# Timeout reduced to 3 seconds
set timeout 3

# The name of this test and the command we will run
set test_file "basic.exp"

proc basic_ls_test {} {
    global test_mnt
    prepare_test "touch" "$test_mnt/abc123"

    run_ci "Exact name lookup" \
	"ls" "$test_mnt/abc123" "$test_mnt/abc123\n"

    run_cs "CS Non-existant file" \
	"ls" "$test_mnt/ABC123" "*No such file or directory*"

    run_ci "Inexact name lookup" \
	"ls" "$test_mnt/ABC123" "$test_mnt/ABC123\n"

    run_ci "Inexact name lookup of inexistant file" \
	"ls" "$test_mnt/ABC132" "*No such file or directory*"


    prepare_test "rm" "$test_mnt/abc123"
}

basic_ls_test

