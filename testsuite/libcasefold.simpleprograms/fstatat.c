/*
 * Copyright (C) 2018 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Gabriel Krisman Bertazi <krisman@collabora.co.uk>
 */

#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>
#include<stdio.h>
#include<string.h>
#include<errno.h>
#include <fcntl.h>
#include<sys/syscall.h>

int main (int argc, char *argv[]) {

	struct stat stat;
	char *dirpath, *fpath;
	int dfd;
	int r;

	dirpath = argv[1];
	fpath = argv[2];

	if (strcmp(dirpath, "PWD") == 0)
		dfd = AT_FDCWD;
	else {
		if (dirpath[0] == '/')
			dfd = open (dirpath, O_DIRECTORY);
		else
			dfd = openat(AT_FDCWD, dirpath, O_DIRECTORY);
		if (dfd <= 0) {
			printf("Error: Can't open parent directory\n");
			return 1;
		}
	}

	r = fstatat(dfd, fpath, &stat, 0);
	if (r) {
		printf("Error: %s\n", strerror(errno));
		return 1;
	}

#ifdef __i686
	r = syscall(SYS_fstatat64, dfd, fpath, &stat, 0);
	if (r) {
		printf("Error on direct fstatat64: %s\n", strerror(errno));
		return 1;
	}
#endif

	printf("Test Completed \n");
	return 0;
}
