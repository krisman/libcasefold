/*
 * Copyright (C) 2018 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Gabriel Krisman Bertazi <krisman@collabora.co.uk>
 */

#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <error.h>
#include <string.h>
#include <unistd.h>
#include <syscall.h>
#include <limits.h>
#include <fcntl.h>
#include <stdint.h>

/* unlink:
 *
 * Testing unlink() is a bit tricky, because of how we are always
 * dealing with a test program being intercepted transparently by
 * libcasefold.  Since libcasefold doesn't provide way to invalidate the
 * cache explicitly, we do some massage to figure out if the unliked
 * dir was really invalidated.
 *
 * The test case does the following:
 *
 * 1) Create a file abc123
 * 2) Try to Create a file ABC123  <- Fails, same name.  (not really necessary)
 * 3) Remove abc123
 * 4) Create a file ABC123  <- Succeeds.
 * 5) Look for a file with the exact name ABC123 with an explicit getdirents()
 *
 * If the caching was done right and correctly invalidated the file
 * after removal in step 3, the file created in step 4 will be called
 * ABC123.  If the cache was not invalidated, the creation in step 4
 * will create again a file with abc123, because it was not removed in
 * the cache after the unlinking.
 *
 * Manually crawling through the directory is required in step 5
 * because, once we are being intercepted by libcasefold, there is no
 * easy way to figure out if we have an exact case match or not.
 */

#define EXACT_CASE 	"abc123"
#define INEXACT_CASE	"ABC123"

struct linux_dirent64
{
	uint64_t  d_ino;    /* 64-bit inode number */
	uint64_t  d_off;    /* 64-bit offset to next structure */
	unsigned short d_reclen; /* Size of this dirent */
	unsigned char  d_type;   /* File type */
	char           d_name[]; /* Filename (null-terminated) */
};

int main (int argc, char *argv[])
{
	char *dir;
	int r, nread;
	char f1[PATH_MAX], f2[PATH_MAX];
	int dfd, fd1, fd2;
	char buffer[BUFSIZ];
	if (argc <= 1) {
		fprintf(stderr, "error: Usage:\n");
		fprintf(stderr, "\t%s <test_dir>\n", argv[0]);
		return 1;
	}
	dir = argv[1];
	r = mkdir(dir, 0777);
	if (r && errno != EEXIST) {
		error(1, errno, "error: could not create %s", dir);
		return 1;
	}

	snprintf(f1, PATH_MAX, "%s/%s", dir, EXACT_CASE);
	snprintf(f2, PATH_MAX, "%s/%s", dir, INEXACT_CASE);

	fd1 = open(f1, O_CREAT, 0777);
	if (fd1 < 0) {
		error(1, errno, "error: create fd1 %s", f1);
		return 1;
	}
	close (fd1);

	fd2 = open(f2, O_CREAT|O_EXCL, 0777);
	if (fd2 > -1) {
		error(1, errno,
		      "error: %s should not be able to be created", f1);
		return 1;
	}

	r = unlink(f1);
	if (r) {
		error(1, errno, "error: Could not unlink %s:", f1);
		return 1;
	}

	fd2 = open(f2, O_CREAT|O_EXCL, 0777);
	if (fd2 < -1) {
		error(1, errno,
		      "error: %s could not be created after unlink", f2);
		return 1;
	}

	dfd = open(dir, O_DIRECTORY);
	if (dfd < 0) {
		error(1, errno,
		      "error: %s could not open the test directory", dir);
		return 1;

	}

	/* sys_getdents64 code adapted from the man page. */
	for ( ; ; ) {
		struct linux_dirent64 *d;
		nread = syscall(SYS_getdents64, dfd, buffer, BUFSIZ);
		if (nread == -1) {
			error(1, errno,
			      "error: %s could not do readdents", dir);
			return 1;
		}

		if (nread == 0) {
			error(1, errno,
			      "error: can't find exact case of recently created file %s", f2);
			return 1;
		}

		for (int bpos = 0; bpos < nread; bpos += d->d_reclen) {
			d = (struct linux_dirent64 *) (buffer + bpos);
			fprintf(stderr, "%s, %s\n", d->d_name, INEXACT_CASE);
			if (strcmp(d->d_name, INEXACT_CASE) == 0) {
				printf("Test Complete\n");
				return 0;
			}
		}
	}
	return 1;
}
