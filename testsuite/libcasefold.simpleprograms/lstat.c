/*
 * Copyright (C) 2018 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Gabriel Krisman Bertazi <krisman@collabora.co.uk>
 */


#define _GNU_SOURCE 1
#include<sys/stat.h>
#include<sys/types.h>

#include<unistd.h>
#include<stdio.h>
#include<string.h>
#include<errno.h>
#include<sys/syscall.h>

int main (int argc, char *argv[]) {
	struct stat stat;
	int r = lstat(argv[1], &stat);
	if (r) {
		printf("Error: %s\n", strerror(errno));
		return 1;
	}

#ifdef __i686__
	struct stat64 stat64;
	r = syscall(SYS_lstat64, argv[1], &stat64);
	if (r) {
		printf("Failed on direct call to lstat64:  %s\n",
		       strerror(errno));
		return 1;
	}
	r = syscall(SYS_lstat, argv[1], &stat);
	if (r) {
		printf("Failed on direct call to lstat64:  %s\n",
		       strerror(errno));
		return 1;
	}
#endif

	printf("Test Completed \n");
	return 0;
}



