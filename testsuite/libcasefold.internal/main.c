/*
 * Copyright (C) 2018 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Gabriel Krisman Bertazi <krisman@collabora.co.uk>
 */

#include "../../include/casefold.h"
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>

int main ()
{
	char *paths[] = {"/home/krisman/mnt-ci",
			"/home/krisman/mnt-ci/subdir",
			"/home/krisman/mnt-ci/subdir/bla",
			 "/home/krisman/mnt/subdir/bla",
			 "/tmp/ci",
			 "/tmp",
			 NULL};
	char *p;
	int r;
	struct dentry *mnt_dentry;

	if (load_mount_points())
		printf("can't load mountpoints\n");

	for (int i = 0 ; paths[i]; i++) {
		int ci = find_absolute_ci_root(paths[i], &mnt_dentry);
		printf("%s %s\n%*s %d\n", paths[i], ci>0? "CI":"CS", ci+1, "^", ci);
	}

	p = "/tmp/test-tmp";
	r = open(p, O_CREAT, S_IRUSR);
	printf("%s %d (%s)\n", p, r, strerror(errno));
	r = unlinkat(0, p, 0);

	p = "/tmp/ci/tMp";
	r = open(p, O_CREAT,S_IRUSR);
	if (r < 0)
		printf("%s %d (%s)\n", p, r, strerror(errno));
	else
		printf("%s %d\n", p, r);
	r = unlinkat(0, p, 0);


	return 0;
}
