# Copyright (C) 2018 Collabora Ltd.
#
# This file is part of the libcasefold.
#
# This file is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston,
# MA 02110-1301, USA.
#
# Author: Gabriel Krisman Bertazi <krisman@collabora.co.uk>

set timeout 3

set libcasefold_path "$objdir/../lib/.libs/libcasefold.so"

# The following function is based on the code from
# binutils/testsuite/lib/binutils-common.exp. The
# original code is Copyrighted to the Free Software
# Foundation, Inc. is licensed under GPL version 3
# and can be found at:
# https://www.gnu.org/software/gdb/current/
proc run_test_program {prog progargs} {
    if ![is_remote host] {
	if {[which $prog] == 0} then {
	    perror "$prog does not exist"
	    return ""
	}
    }

    send_log "$prog $progargs\n"
    verbose "$prog $progargs"

    # Gotta quote dollar-signs because they get mangled by the
    # shell otherwise.
    regsub -all "\\$" "$progargs" "\\$" progargs

    set state [remote_exec host $prog $progargs]
    set exec_output [prune_warnings [lindex $state 1]]
    set err_output [prune_warnings [lindex $state 2]]
    if {![string match "" $exec_output]} then {
	send_log "$exec_output\n"
	verbose "$exec_output"
    } else {
	if { [lindex $state 0] != 0 } {
	    set exec_output "$prog exited with status [lindex $state 0]"
	    send_log "$exec_output\n"
	    verbose "$exec_output"
	}
    }
    return $exec_output
}

proc run_ci_test_program {prog progargs} {
    global libcasefold_path test_mnt

    set orig_ld_preload "[getenv LD_PRELOAD]"
    set orig_cimountpoints "[getenv CIMOUNTPOINTS]"
    setenv LD_PRELOAD "$libcasefold_path:$orig_ld_preload"
    setenv CI_MOUNTPOINTS "$test_mnt"
    set result [run_test_program $prog $progargs]
    setenv LD_PRELOAD "$orig_ld_preload"
    setenv CI_MOUNTPOINTS "$orig_cimountpoints"
    return $result
}

proc run_cs {test_name prog args output} {
    set exec_output [run_test_program $prog $args]

    if ![string match $output $exec_output] then {
	send_log $exec_output
	fail $test_name
	return
    }
    pass $test_name
}

proc run_ci {test_name prog args output} {
    set exec_output [run_ci_test_program $prog $args]

    if ![string match $output $exec_output] then {
	send_log $exec_output
	fail $test_name
	return
    }
    pass $test_name
}

proc run_cs_on_dir {test_name dir prog args output} {
    set cwd [pwd]
    cd $dir
    run_cs $test_name $prog $args $output
    cd $cwd
}

proc run_ci_on_dir {test_name dir prog args output} {
    set cwd [pwd]
    cd $dir
    run_ci $test_name $prog $args $output
    cd $cwd
}

proc prepare_test {cmdline args} {
    spawn $cmdline $args
}

# test_mnt should be set in global config, such that it could be easily
# replaced by the user.  But this quick hack allow us to drop dependency
# on fileutil, which is too old in jessie.
if {[which "mktemp"] == 0} then {
    perror "$prog does not exist"
    return ""
}
set test_mnt [string  trim [run_test_program "mktemp" "-d"] "\n"]
puts "$test_mnt"
